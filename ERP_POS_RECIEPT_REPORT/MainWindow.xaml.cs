﻿using ERP_POS_RECIEPT_REPORT.ReportGraphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ERP_POS_RECIEPT_REPORT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var Data = new Report_POS()
            {

            };
            Data.Company = new CompanyInfo()
            {
                Activity = "مركو تجاري ",
                Address = "حي 400 سكن الوادي",
                Logo = "",
                Phone = "(+213)664964084",
                Title = "Gama DEV"

            };
            Data.SaleOrder = new POSOrder()
            {
                Client = "mohammed elhabib kahla",
                SaleLines = new List<SaleLine>() {
                new SaleLine()
                {
                    ProductName="Product 1",
                    Price=20.00m,
                    Discount=20m,
                    Quantity=10,
                    SubTotal=200.00m
                },     new SaleLine()
                {
                    ProductName="Product 1",
                    Price=20.00m,
                    Quantity=10,
                    SubTotal=200.00m
                },     new SaleLine()
                {
                    ProductName="Product 1",
                    Price=20.00m,
                    Quantity=10,
                    SubTotal=200.00m
                },     new SaleLine()
                {
                    ProductName="Product 1",
                    Price=20.00m,
                    Quantity=10,
                    SubTotal=200.00m
                },     new SaleLine()
                {
                    ProductName="Product 1",
                    Price=20.00m,
                    Quantity=10,
                    SubTotal=200.00m
                },     new SaleLine()
                {
                    ProductName="Produciiiiiiiiiiiiiiiiiiiiiiiioiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiit 1",
                    Price=20.00m,
                    Quantity=10,
                    SubTotal=200.00m, Discount=5

                }

                },
                Date = DateTime.Now,
                CustomerInfo = new CustomerInfo()
                {
                    Balance = 50,
                    LoyaltyPoints = 2,
                    Name = "Amara"

                }
                ,
                UserName = "Omar",
                ID = "865656565650"
                ,
                RoundingTotAmount = 20,
                PaymentAmount = 600.00m
            };
            Data.IsPrintExtraInfo = true;
            Data.IsPrintCompanyLogo = false;
            var prints = new Report_Graphics_80_EX01(Data);
            ImageSourceConverter c = new ImageSourceConverter();
            var bitmap = prints.GenerateTicketGraphics();
            var a = ImageSourceFromBitmap(bitmap);

            THIS.Source = a;
        }
        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject([In] IntPtr hObject);

        public ImageSource ImageSourceFromBitmap(Bitmap bmp)
        {
            var handle = bmp.GetHbitmap();
            try
            {
                return Imaging.CreateBitmapSourceFromHBitmap(handle, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
            finally { DeleteObject(handle); }
        }
    }
}
