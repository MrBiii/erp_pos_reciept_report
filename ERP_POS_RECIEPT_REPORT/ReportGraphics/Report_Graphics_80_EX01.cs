﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_POS_RECIEPT_REPORT.ReportGraphics
{
    public abstract class Report_Graphics_80_EX01
    {
        Graphics graphics { get; set; }
        PrintDocument doc { get; set; }
        Report_POS Data { get; set; }
        private int YStart = 5;
        private int XStart = 5;
        private int XEnd;
        //  private int NextLine = 72;
        private StringFormat sf;
        private Pen p;
        private Rectangle Qre;
        private Rectangle Prec;
        private Rectangle Drec;
        private Rectangle Mrec;
        private Font ff;
        private int PrixWidth = 48;
        private int NomWidth = 145;
        private int QteWidth = 28;
        private int MontWidth = 60;
        private int Virgule = 2;
        protected Lang lang;
        protected Dictionary<string, string> _dictionary;
        public Report_Graphics_80_EX01(Report_POS reportData)
        {
            Data = reportData;
            sf = new StringFormat();
            p = new Pen(Brushes.Black, 0.25F);
            XEnd = 285;
            ff = new Font("Calibri", 8, FontStyle.Bold);

            //if (lang != Lang.ar)
            //    _dictionary = new Dictionary<string, string>()
            //    {
            //        ["ID"] = "Voucher number :",
            //        ["Date"] = "Date :",
            //        ["Quantity"] = "Quantity",
            //        ["Designation"] = "Designation",
            //        ["price"] = "Price",
            //        ["amount"] = "Amount",
            //        ["Numberofproducts"] = "Number of products:",
            //        ["Total"] = "Total",
            //        ["RoundedExchange"] = "Rounded exchange",
            //        ["Pay"] = "Pay",
            //        ["user"] = "The user :",
            //        ["VisitMessage"] = "Your visit Enjoy us",
            //        ["Notice"] = "Notice: Any protest does not exceed 24 hours",
            //        ["Totalofproducts"] = "Total  of products : ",
            //        ["Client"] = "Client : "

            //    };
            //else
            //    _dictionary = new Dictionary<string, string>()
            //    {
            //        ["ID"] = ": رقم الوصل",
            //        ["Date"] = ": التاريخ",
            //        ["Quantity"] = "الكمية",
            //        ["Designation"] = "التعييـــن",
            //        ["price"] = "السعر",
            //        ["amount"] = "المبلغ",
            //        ["Numberofproducts"] = "عدد المنتجات : ",
            //        ["Total"] = "المجموع",
            //        ["RoundedExchange"] = "تقريب الصرف",
            //        ["Pay"] = "دفع",
            //        ["user"] = ": المستخدم",
            //        ["VisitMessage"] = "زيارتكم تسرنــا",
            //        ["Notice"] = "تنبيه : أي إحتجاج لا يتجاوز 24 ساعة",
            //        ["Totalofproducts"] = "عدد الاجمالي للمنتوجات :",
            //        ["Client"] = ": العميل"

            //    };
        }

        public Bitmap GenerateTicketGraphics()
        {


            Bitmap bmp = new Bitmap(290, 700);

            var g = Graphics.FromImage(bmp);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

            // Start Drawing ..... 
            // Drawing Header
            _DrawHeader(g);
            // Drawing Conents
            _DrawContent(g);
            // Drawing Footer
            _DrawFooter(g);


            return bmp;
        }

        private void _DrowBarcode(Graphics g)
        {

            //try
            //{
            //    SSControls.an13 ean13 = null;
            //    ean13 = new SSControls.Ean13();
            //    ean13.CountryCode = TicketNumber.Substring(0, 2);
            //    ean13.ManufacturerCode = TicketNumber.Substring(2, 5);
            //    ean13.ProductCode = TicketNumber.Substring(7, 5);
            //    ean13.ChecksumDigit = "1";
            //    ean13.Scale = 1f;
            //    ean13.CalculateChecksumDigit();

            //    ean13.DrawEan13BarcodeNoNumbers(g, new System.Drawing.Point(10, Convert.ToInt32((line) * 0.254)), "", 20);
            //    g.Dispose();
            //}
            //catch (Exception) { };



        }

        private void _DrawContent(Graphics g)
        {
            YStart += ff.Height;
            int numTitleWith = (int)g.MeasureString(_dictionary["ID"], ff).Width + 3;
            int numWith = (int)g.MeasureString(Data.SaleOrder.ID, ff).Width + 3;
            var XStarts = (lang == Lang.ar) ? ComputeXStartsRight(false, numWith, numTitleWith) : ComputeXStartsLeft(false, numTitleWith, numWith);
            Prec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[1], YStart, numWith, ff.Height);
            Drec = new Rectangle((lang == Lang.ar) ? XStarts[1] : XStarts[0], YStart, numTitleWith, ff.Height);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Far : StringAlignment.Near;
            g.DrawString(_dictionary["ID"], ff, Brushes.Black, (RectangleF)Drec, sf);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;
            g.DrawString(Data.SaleOrder.ID, ff, Brushes.Black, (RectangleF)Prec, sf);
            YStart += ff.Height;
            int dateTitleWith = (int)g.MeasureString(_dictionary["Date"], ff).Width + 3;
            int dateWith = (int)g.MeasureString(Data.SaleOrder.Date.ToString("HH:MM:ss  yyyy/mm/dd"), ff).Width + 3;
            XStarts = (lang == Lang.ar) ? ComputeXStartsRight(false, dateWith, dateTitleWith) : ComputeXStartsLeft(false, dateTitleWith, dateWith);
            Prec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[1], YStart, dateWith, ff.Height);
            Drec = new Rectangle((lang == Lang.ar) ? XStarts[1] : XStarts[0], YStart, dateTitleWith, ff.Height);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Far : StringAlignment.Near;
            g.DrawString(_dictionary["Date"], ff, Brushes.Black, (RectangleF)Drec, sf);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;
            g.DrawString(Data.SaleOrder.Date.ToString("HH:MM:ss  yyyy/mm/dd"), ff, Brushes.Black, (RectangleF)Prec, sf);
            YStart += ff.Height;
            int clientTitleWith = (int)g.MeasureString(_dictionary["Client"], ff).Width + 3;
            int clientWith = (int)g.MeasureString(Data.SaleOrder.Client, ff).Width + 3;
            XStarts = (lang == Lang.ar) ? ComputeXStartsRight(false, clientWith, clientTitleWith) : ComputeXStartsLeft(false, clientTitleWith, clientWith);
            Prec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[1], YStart, clientWith, ff.Height);
            Drec = new Rectangle((lang == Lang.ar) ? XStarts[1] : XStarts[0], YStart, clientTitleWith, ff.Height);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Far : StringAlignment.Near;
            g.DrawString(_dictionary["Client"], ff, Brushes.Black, (RectangleF)Drec, sf);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;
            g.DrawString(Data.SaleOrder.Client, ff, Brushes.Black, (RectangleF)Prec, sf);
            ////// Draw Table
            _DrawTableSaleOrdeLines(g);

        }

        private void _DrawTableSaleOrdeLines(Graphics g)
        {

            int RowHeight = ff.Height;
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            YStart = YStart + RowHeight;
            var XStarts = (lang == Lang.ar) ? ComputeXStartsRight(false, MontWidth, PrixWidth, NomWidth, QteWidth) : ComputeXStartsLeft(false, QteWidth, NomWidth, PrixWidth, MontWidth);
            Mrec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[3], YStart + RowHeight, MontWidth, RowHeight);
            Prec = new Rectangle((lang == Lang.ar) ? XStarts[1] : XStarts[2], YStart + RowHeight, PrixWidth, RowHeight);
            //if (PrintPrixUnit != "1")
            //    Drec = new Rectangle(StartXMont + MontWidth, NextLine + RowWidth, NomWidth + PrixWidth, RowWidth);
            //else
            Drec = new Rectangle((lang == Lang.ar) ? XStarts[2] : XStarts[1], YStart + RowHeight, NomWidth, RowHeight);
            Qre = new Rectangle((lang == Lang.ar) ? XStarts[3] : XStarts[0], YStart + RowHeight, QteWidth, RowHeight);

            Pen p = new Pen(Brushes.Black, 0.25F);
            g.DrawRectangle(p, Qre);// e.Graphics.FillRectangle(Brushes.Gray, Qrec);
            g.DrawString(_dictionary["Quantity"], ff, Brushes.Black, (RectangleF)Qre, sf);

            g.DrawRectangle(p, Drec);// e.Graphics.FillRectangle(Brushes.Gainsboro, Drec);
            g.DrawString(_dictionary["Designation"], ff, Brushes.Black, (RectangleF)Drec, sf);


            g.DrawRectangle(p, Prec);// e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);

            g.DrawString(_dictionary["price"], ff, Brushes.Black, (RectangleF)Prec, sf);

            g.DrawRectangle(p, Mrec);// e.Graphics.FillRectangle(Brushes.Gainsboro, Mrec);
            g.DrawString(_dictionary["amount"], ff, Brushes.Black, (RectangleF)Mrec, sf);
            YStart = YStart + RowHeight;

            sf.FormatFlags = (lang == Lang.ar) ? StringFormatFlags.DirectionRightToLeft : sf.FormatFlags;
            foreach (var td in Data.SaleOrder.SaleLines)
            {
                int Height = ff.Height;
                var size = g.MeasureString(td.ProductName, ff);
                if (Math.Round(size.Width) > MontWidth)
                {
                    var num = (int)Math.Round((size.Width / NomWidth));
                    Height = RowHeight * num;
                }
                if (td.Discount > 0)
                    Height += RowHeight;
                //if (td.iNTEX == PrintFromIndex)
                //{
                //bool OneLine = true;
                //if ((td.Observ != "" || td.remise > 0) && Properties.Settings.Default.DoubleLineOnRemise)
                //{
                //    OneLine = false;
                //}
                sf.LineAlignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;

                sf.FormatFlags = StringFormatFlags.NoClip;
                XStarts = (lang == Lang.ar) ? ComputeXStartsRight(false, MontWidth, PrixWidth, NomWidth, QteWidth) : ComputeXStartsLeft(false, QteWidth, NomWidth, PrixWidth, MontWidth);
                Mrec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[3], YStart + RowHeight, MontWidth, Height);
                Prec = new Rectangle((lang == Lang.ar) ? XStarts[1] : XStarts[2], YStart + RowHeight, PrixWidth, Height);
                //if (PrintPrixUnit != "1")
                //    Drec = new Rectangle(StartXMont + MontWidth, NextLine + RowWidth, NomWidth + PrixWidth, RowWidth);
                //else
                Drec = new Rectangle((lang == Lang.ar) ? XStarts[2] : XStarts[1], YStart + RowHeight, NomWidth, Height);
                Qre = new Rectangle((lang == Lang.ar) ? XStarts[3] : XStarts[0], YStart + RowHeight, QteWidth, Height);
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;
                string qteN2 = "N" + Virgule;
                if (td.Quantity == Math.Floor(td.Quantity)) qteN2 = "N0";
                g.DrawRectangle(p, Qre);
                g.DrawString(td.Quantity.ToString(qteN2), ff, Brushes.Black, (RectangleF)Qre, sf);

                sf.Alignment = (lang == Lang.ar) ? StringAlignment.Far : StringAlignment.Near;
                g.DrawRectangle(p, Drec);

                var productname = td.ProductName;
                if (td.Discount > 0)
                {
                    productname += "\n R % : " + td.Discount.ToString("N2");
                }
                g.DrawString(productname, ff, Brushes.Black, (RectangleF)Drec, sf);
                //var size = g.MeasureString(td.ProductName, ff);
                //if (size.Height > RowHeight)
                //{
                //    int num = (int)size.Height / RowHeight;
                //    RowHeight *= RowHeigderc;
                //}
                //if (td.Discount > 0)
                //    RowHeight += RowHeight;
                //if (!OneLine)
                //{


                //    g.DrawString("  " + td., ff, Brushes.Black, (RectangleF)Drec, sf);
                //   sf.LineAlignment = (lang == Lang.ar) ? StringAlignment.Far : StringAlignment.Near;
                //if (td.Discount > 0)
                //    g.DrawString("R% : " + td.Discount.ToString("N2"), ff, Brushes.Black, (RectangleF)Drec, sf);
                // }
                //  sf.LineAlignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;


                sf.Alignment = StringAlignment.Far;

                // if (PrintPrixUnit == "1")
                g.DrawRectangle(p, Prec);
                ///if (PrintPrixUnit == "1")

                g.DrawString((td.Price).ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);

                g.DrawRectangle(p, Mrec);
                g.DrawString((td.SubTotal).ToString("N2"), ff, Brushes.Black, (RectangleF)Mrec, sf);

                this.YStart += Height;
            }
        }

        private void _DrawFooter(Graphics g)
        {

            // g.MeasureString();


            int NumberofproductsWith = 142;
            int TotolWith = 70;
            int textWith = 100;
            //// line  
            this.YStart += ff.Height;
            var XStarts = (lang == Lang.ar) ? ComputeXStartsLeft(true, MontWidth, TotolWith, NumberofproductsWith) : ComputeXStartsRight(true, NumberofproductsWith, TotolWith, (MontWidth - 1));
            Prec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[2], this.YStart, MontWidth, ff.Height);

            Drec = new Rectangle(XStarts[1], this.YStart, TotolWith, ff.Height);

            Qre = new Rectangle((lang == Lang.ar) ? XStarts[2] : XStarts[0], this.YStart, NumberofproductsWith, ff.Height);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Far : StringAlignment.Near;
            //e.Graphics.DrawRectangle(p, Qrec); //e.Graphics.FillRectangle(Brushes.Gray, Qrec);
            g.DrawString(_dictionary["Numberofproducts"] + Data.SaleOrder.SaleLines.Count, ff, Brushes.Black, (RectangleF)Qre, sf);

            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;
            //e.Graphics.DrawRectangle(p, Drec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Drec);
            g.DrawString(_dictionary["Total"], ff, Brushes.Black, (RectangleF)Drec, sf);
            sf.Alignment = StringAlignment.Far;
            g.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);
            g.DrawString((Data.SaleOrder.SaleLines.Sum(x => x.SubTotal)).ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (Data.SaleOrder.RoundingTotAmount != 0.00m)
            {
                this.YStart += ff.Height;
                int totalproduct = textWith + 12;
                XStarts = (lang == Lang.ar) ? ComputeXStartsLeft(true, MontWidth, textWith, totalproduct) : ComputeXStartsRight(true, totalproduct, textWith, (MontWidth - 1));
                Prec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[2], this.YStart, MontWidth, ff.Height);
                Drec = new Rectangle(XStarts[1], this.YStart, textWith, ff.Height);
                Qre = new Rectangle((lang == Lang.ar) ? XStarts[2] : XStarts[0], this.YStart, totalproduct, ff.Height);
                sf.Alignment = (lang == Lang.ar) ? StringAlignment.Far : StringAlignment.Near;
                //e.Graphics.DrawRectangle(p, Qrec); //e.Graphics.FillRectangle(Brushes.Gray, Qrec);
                g.DrawString(_dictionary["Totalofproducts"] + Data.SaleOrder.SaleLines.Sum(x => x.Quantity), ff, Brushes.Black, (RectangleF)Qre, sf);
                sf.Alignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;
                g.DrawString(_dictionary["RoundedExchange"], ff, Brushes.Black, (RectangleF)Drec, sf);
                sf.Alignment = StringAlignment.Far;
                g.DrawRectangle(p, Prec);
                g.DrawString((Data.SaleOrder.RoundingTotAmount).ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);

            }
            ////////////// Payment Amount
            if (Data.SaleOrder.PaymentAmount != 0.0m)
            {
                this.YStart += ff.Height;
                XStarts = (lang == Lang.ar) ? ComputeXStartsLeft(true, MontWidth, textWith) : ComputeXStartsRight(true, textWith, MontWidth - 1);
                Prec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[1], this.YStart, MontWidth, ff.Height);
                Drec = new Rectangle((lang == Lang.ar) ? XStarts[1] : XStarts[0], this.YStart, textWith, ff.Height);
                sf.Alignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;
                g.DrawString(_dictionary["Pay"], ff, Brushes.Black, (RectangleF)Drec, sf);
                sf.Alignment = StringAlignment.Far;
                g.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);
                g.DrawString(Data.SaleOrder.PaymentAmount.ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);

            }

            this.YStart += ff.Height;
            // XStarts = ComputeXStartsLeft(XStart, MontWidth, textWith);
            XStarts = (lang == Lang.ar) ? ComputeXStartsRight(true, textWith, 50) : ComputeXStartsLeft(true, 50, textWith - 1);

            //  XStarts = ComputeXStartsRight(textWith, 50);
            Prec = new Rectangle((lang == Lang.ar) ? XStarts[0] : XStarts[1], this.YStart, textWith, ff.Height);
            Drec = new Rectangle((lang == Lang.ar) ? XStarts[1] : XStarts[0], this.YStart, 50, ff.Height);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Near : StringAlignment.Far;
            g.DrawString(_dictionary["user"], ff, Brushes.Black, (RectangleF)Drec, sf);
            sf.Alignment = (lang == Lang.ar) ? StringAlignment.Far : StringAlignment.Near;
            // g.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);
            g.DrawString(Data.SaleOrder.UserName, ff, Brushes.Black, (RectangleF)Prec, sf);


            //if (

            //{
            //    Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //    Qrec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 103 + 100, RowHeight);
            //    sf.Alignment = StringAlignment.Far;
            //    e.Graphics.DrawString(string.Format("تم إقتطاع من بطاقة الرصيد:", ppCardBalance), ff, Brushes.Black, (RectangleF)Qrec, sf);

            //    sf.Alignment = StringAlignment.Near;
            //    e.Graphics.DrawString(ppCardBalance.ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);
            //    e.Graphics.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);

            //    NextLine = NextLine + RowHeight;

            //    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //    Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //    Qrec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 103 + 100, RowHeight);
            //    sf.Alignment = StringAlignment.Far;
            //    e.Graphics.DrawString(string.Format("باقي الرصيد بالبطاقة:", ppCardBalance), ff, Brushes.Black, (RectangleF)Qrec, sf);
            //    var cardNewBalance = ppCardBalance - Total;
            //    sf.Alignment = StringAlignment.Near;
            //    e.Graphics.DrawString((cardNewBalance > 0 ? cardNewBalance : 0).ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);
            //    e.Graphics.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);

            //    NextLine = NextLine + RowHeight;

            //    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //    if (cardNewBalance < 0)
            //    {
            //        Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //        Qrec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 103 + 100, RowHeight);
            //        sf.Alignment = StringAlignment.Far;
            //        e.Graphics.DrawString(string.Format("المبلغ المدفوع نقداً:", ppCardBalance), ff, Brushes.Black, (RectangleF)Qrec, sf);
            //        sf.Alignment = StringAlignment.Near;
            //        e.Graphics.DrawString((Math.Abs(cardNewBalance)).ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);
            //        e.Graphics.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);

            //        NextLine = NextLine + RowHeight;
            //    }
            //}

            //if (PrepaidCard != null)
            //{
            //    Qrec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 103 + 100, RowHeight);
            //    sf.Alignment = StringAlignment.Near;
            //    e.Graphics.DrawString(string.Format("رقم البطاقة : {0}", PrepaidCard.Card.SerialNumber), ff, Brushes.Black, (RectangleF)Qrec, sf);
            //    NextLine = NextLine + RowHeight;
            //    Qrec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 103 + 100, RowHeight);
            //    sf.Alignment = StringAlignment.Near;
            //    e.Graphics.DrawString(string.Format("إسم المالك : {0}", PrepaidCard.Owner.Title), ff, Brushes.Black, (RectangleF)Qrec, sf);
            //    NextLine = NextLine + RowHeight;
            //}
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //Drec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 100, RowHeight);
            //Qrec = new Rectangle(StartXMont + 75 + 103, NextLine + RowHeight, 103, RowHeight);
            //sf.Alignment = StringAlignment.Far;

            //if (remise > 0)
            //{
            //    sf.Alignment = StringAlignment.Near;
            //    e.Graphics.DrawString("التخفيض", ff, Brushes.Black, (RectangleF)Drec, sf);

            //    sf.Alignment = StringAlignment.Far;
            //    e.Graphics.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);
            //    e.Graphics.DrawString((remise).ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);

            //    NextLine = NextLine + RowHeight;
            //    Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //    Drec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 100, RowHeight);
            //    Qrec = new Rectangle(StartXMont + 75 + 103, NextLine + RowHeight, 103, RowHeight);

            //    sf.Alignment = StringAlignment.Near;
            //    e.Graphics.DrawString("المجموع - ت", ff, Brushes.Black, (RectangleF)Drec, sf);

            //    sf.Alignment = StringAlignment.Far;
            //    e.Graphics.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);
            //    e.Graphics.DrawString((Total - remise).ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);
            //    NextLine = NextLine + RowHeight;
            //    Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //    Drec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 100, RowHeight);
            //    Qrec = new Rectangle(StartXMont + 75 + 103, NextLine + RowHeight, 103, RowHeight);
            //}
            ////e.Graphics.DrawRectangle(p, Qrec); //e.Graphics.FillRectangle(Brushes.Gray, Qrec);

            //sf.Alignment = StringAlignment.Near;
            ////e.Graphics.DrawRectangle(p, Drec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Drec);

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //Drec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 100, RowHeight);
            //Qrec = new Rectangle(StartXMont + 75 + 103, NextLine + RowHeight, 103, RowHeight);
            //sf.Alignment = StringAlignment.Near;

            //if (montPayer != 0 && Total - remise < montPayer)
            //{
            //    e.Graphics.DrawString("الصرف", ff, Brushes.Black, (RectangleF)Drec, sf);

            //    sf.Alignment = StringAlignment.Far;
            //    e.Graphics.DrawRectangle(p, Prec); //e.Graphics.FillRectangle(Brushes.Gainsboro, Prec);
            //    e.Graphics.DrawString((montPayer - Total + remise).ToString("N2"), ff, Brushes.Black, (RectangleF)Prec, sf);
            //    NextLine = NextLine + RowHeight;
            //}
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //Drec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 100, RowHeight);
            //Qrec = new Rectangle(StartXMont, NextLine + RowHeight, 290, RowHeight);

            //sf.Alignment = StringAlignment.Near;
            //sf.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            //e.Graphics.DrawString("   المستخدم :" + UserName + " // " + Environment.MachineName, ff, Brushes.Black, (RectangleF)Qrec, sf);

            //NextLine = NextLine + RowHeight;
            //Prec = new Rectangle(StartXMont, NextLine + RowHeight, 70, RowHeight);
            //Drec = new Rectangle(StartXMont + 75, NextLine + RowHeight, 100, RowHeight);
            //Qrec = new Rectangle(StartXMont, NextLine + RowHeight, 290, RowHeight);

            //if (cccpoint != 0)
            //{
            //    sf.Alignment = StringAlignment.Far;

            //    e.Graphics.DrawString("عدد النقاط لـ:" + cccNom + " = " + cccpoint, ff, Brushes.Black, (RectangleF)Qrec, sf);

            //    NextLine = NextLine + RowHeight;
            //}
            ////    ff = new Font("Mudir MT", 10, FontStyle.Bold);
            //sf = new StringFormat();
            //sf.Alignment = StringAlignment.Center;
            //sf.LineAlignment = StringAlignment.Center;

            //Inforec = new Rectangle(StartXMont, NextLine + RowHeight, 280, ff.Height);
            ////    e.Graphics.DrawString("Merci pour votre visite.", ff, System.Drawing.Brushes.Black, Inforec, sf);
            //e.Graphics.DrawString("زيـــارتٌكمْ تسرٌنَـــا", ff, System.Drawing.Brushes.Black, Inforec, sf);
            //NextLine = NextLine + 25;

            //string note = "تنبيه: أي إحتجاج لا يتجاوز 24 ساعة";
            //if (File.Exists("./Option/TicketNote.ftec"))
            //    note = File.ReadAllText("./Option/TicketNote.ftec") == "" ? "تنبيه: أي إحتجاج لا يتجاوز 24 ساعة" : File.ReadAllText("./Option/TicketNote.ftec");

            //Inforec = new Rectangle(StartXMont, NextLine + RowHeight, 280, ff.Height);
            //e.Graphics.DrawString(note, ff, System.Drawing.Brushes.Black, Inforec, sf);
            //NextLine = NextLine + 40;
            //e.PageSettings.PaperSize = new PaperSize("Ticket", 500, NextLine + 120);

            //Inforec = new Rectangle(StartXMont, NextLine + 38, 300, ff.Height);
            //ff = new Font("Times New Roman", 9, FontStyle.Regular);

            //e.Graphics.DrawString("www.gamadev.com", ff, System.Drawing.Brushes.Black, Inforec, sf);

            //e.Graphics.DrawLine(Pens.Black, new Point(StartXMont, NextLine + 55), new Point(315 + StartXMont, NextLine + 55));
            //drawCodeBar(sender, e, NextLine, StartXMont);

            this.YStart += ff.Height;
            sf.Alignment = StringAlignment.Center;
            Drec = new Rectangle(XStart, this.YStart, XEnd, ff.Height);
            g.DrawString(_dictionary["VisitMessage"], ff, Brushes.Black, Drec, sf);
            this.YStart += ff.Height;
            sf.Alignment = StringAlignment.Center;
            Drec = new Rectangle(XStart, this.YStart, XEnd, ff.Height);
            g.DrawString(_dictionary["Notice"], ff, Brushes.Black, Drec, sf);
            // Drawing Footer
            _DrowBarcode(g);

            this.YStart += ff.Height;
            sf.Alignment = StringAlignment.Center;
            Drec = new Rectangle(XStart, this.YStart, XEnd, ff.Height);
            g.DrawString("www.gamadev.com", ff, Brushes.Black, Drec, sf);

            this.YStart += ff.Height + 2;
            g.DrawLine(p, new PointF(XStart, this.YStart), new PointF(XStart + XEnd, this.YStart));
        }

        private void _DrawHeader(Graphics g)
        {


            if (Data.IsPrintCompanyLogo)
            {
                int logoWith = 180;
                int logoHeigth = 60;
                this.YStart += logoHeigth;
                Rectangle rectLogo = new Rectangle(XStart + 40, this.YStart, logoWith, logoHeigth);
                g.DrawImageUnscaledAndClipped(Base64ToImage(Data.Company.Logo), rectLogo);
            }


            // Print CompanyName
            /// Alignment
            sf.Alignment = StringAlignment.Center;
            Font font = new Font("Cairo", 22.28f, FontStyle.Bold, GraphicsUnit.World);
            Rectangle rect = new Rectangle(XStart, this.YStart, XEnd, font.Height);
            g.DrawString(Data.Company.Title, font, Brushes.Black, rect, sf);


            // Printing Address
            /// Changing location
            this.YStart += font.Height;
            rect = new Rectangle(XStart, this.YStart, XEnd, font.Height);
            /// changing font size
            font = new Font("Cairo", 13f);
            g.DrawString(Data.Company.Address, font, Brushes.Black, rect, sf);


            if (Data.IsPrintExtraInfo)
            {

                // Printing Phone
                /// Changing location
                this.YStart += font.Height;
                rect = new Rectangle(XStart, this.YStart, XEnd, font.Height);
                /// changing font size
                font = new Font("Cairo", 13f);
                g.DrawString(Data.Company.Phone, font, Brushes.Black, rect, sf);

                // Drawing separator
                /// Changing location
                this.YStart += font.Height + 8;
                rect = new Rectangle(XStart, this.YStart, XEnd, font.Height);
                Pen pen = new Pen(Brushes.Black, 2f);
            }

            g.DrawLine(p, new PointF(XStart, this.YStart), new PointF(XStart + XEnd, this.YStart));
        }
        public Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }

        }
        public int[] ComputeXStartsLeft(bool spase = false, params int[] values)
        {
            int spaseWith = spase ? 5 : 0;
            var startsX = new int[values.Length];
            startsX[0] = XStart - 1;
            for (int i = 1; i < values.Length; i++)
            {
                startsX[i] += startsX[i - 1] + values[i - 1] + spaseWith;
            }
            return startsX;
        }
        public int[] ComputeXStartsRight(bool spase = false, params int[] values)
        {
            int spaseWith = spase ? 5 : 0;
            var startsX = new int[values.Length];
            startsX[values.Length - 1] = XEnd - values[values.Length - 1] - 1;
            for (int i = values.Length - 2; i >= 0; i--)
            {
                startsX[i] += startsX[i + 1] - values[i] - spaseWith;
            }
            return startsX;
        }
        public enum Lang
        {

            ar = 0,
            en = 1,
            fr = 2
        }
    }
}
