﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_POS_RECIEPT_REPORT.ReportGraphics
{
    public class Report_Graphics_80_EX01_EN : Report_Graphics_80_EX01
    {

        public Report_Graphics_80_EX01_EN(Report_POS reportData) : base(reportData)
        {
            _dictionary = new Dictionary<string, string>()
            {
                ["ID"] = "Voucher number :",
                ["Date"] = "Date :",
                ["Quantity"] = "Quantity",
                ["Designation"] = "Designation",
                ["price"] = "Price",
                ["amount"] = "Amount",
                ["Numberofproducts"] = "Number of products:",
                ["Total"] = "Total",
                ["RoundedExchange"] = "Rounded exchange",
                ["Pay"] = "Pay",
                ["user"] = "The user :",
                ["VisitMessage"] = "Your visit Enjoy us",
                ["Notice"] = "Notice: Any protest does not exceed 24 hours",
                ["Totalofproducts"] = "Total  of products : ",
                ["Client"] = "Client : "

            };
            lang = Lang.en;
        }
    }
}
