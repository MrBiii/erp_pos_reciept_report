﻿using System.Drawing;

namespace ERP_POS_RECIEPT_REPORT
{
    public class CompanyInfo
    {
        public string Logo { get; set; }

        public string Title { get; set; }
        public string Address { get; set; }
        public string Activity { get; set; }

        public string Phone { get; set; }
  
    }
}