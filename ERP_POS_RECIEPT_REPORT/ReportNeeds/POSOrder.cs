﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_POS_RECIEPT_REPORT
{
    public class POSOrder
    {
        public POSOrder()
        {

        }

        public string ID { get; set; }
        public DateTime Date { get; set; }
        public CustomerInfo CustomerInfo { get; set; }
        public List<SaleLine> SaleLines { get; set; }
        public decimal RoundingTotAmount;
        public decimal PaymentAmount { get; set; }

        public String UserName { get; set; }
        public string Client { get; set; }
    }
}
