﻿namespace ERP_POS_RECIEPT_REPORT
{
    public class SaleLine
    {
        public double Quantity { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
    }
}