﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_POS_RECIEPT_REPORT
{
    public class CustomerInfo
    {
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public int LoyaltyPoints { get; set; }
           
    }
}
