﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_POS_RECIEPT_REPORT
{
    public class Report_POS
    {
        public CompanyInfo Company { get; set; }
        public POSOrder SaleOrder { get; set; }
        
        public string FooterText { get; set; }

        // Print Company logo
        public bool IsPrintCompanyLogo { get; set; } = true;

        /// <summary>
        /// Print full company contact information.
        /// </summary>
        public bool IsPrintExtraInfo { get; set; } = true;

        /// <summary>
        /// Print Customer balance
        /// </summary>
        public bool IsPrintBalance { get; set; } = false;

        public Report_POS()
        {

        }

        public Report_POS(CompanyInfo companyInfo,POSOrder saleOrder,string footerText,bool printLogo=true,bool printExtraInfo = true, bool printBalance = false)
        {

        }


    }
}
